//
// Created by Luka Prebil Grintal on 21. 04. 20.
//
#ifndef OPTIONAL__BAD_OPTIONAL_ACCESS_H_
#define OPTIONAL__BAD_OPTIONAL_ACCESS_H_

#include <exception>

namespace lpg {
class bad_optional_access : public std::exception {
   public:
	bad_optional_access() = default;
	[[nodiscard]] const char *what() const noexcept override { return "bad optional access"; }
};
} // end namespace lpg

#endif//OPTIONAL__BAD_OPTIONAL_ACCESS_H_
