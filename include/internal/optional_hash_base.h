//
// Created by luka on 23. 04. 20.
//

#ifndef OPTIONAL_INTERNAL_OPTIONAL_HASH_BASE_H_
#define OPTIONAL_INTERNAL_OPTIONAL_HASH_BASE_H_
#include <cstddef>
#include <type_traits>

namespace lpg {

// Forward declare lpg::optional
template<typename T>
class optional;

namespace internal {
using std::size_t;

template<typename Key>
constexpr static void assert_hash_enabled() {
	static_assert(std::is_convertible_v<decltype(std::declval<std::hash<Key> &>()(std::declval<Key const &>())), size_t>, "std::hash<Key> does not provide a call operator");
	static_assert(std::is_default_constructible_v<std::hash<Key>>, "std::hash<Key> must be default constructible");
	static_assert(std::is_copy_constructible_v<std::hash<Key>>, "std::hash<Key> must be copy constructible");
	static_assert(std::is_copy_assignable_v<std::hash<Key>>, "std::hash<Key> must be copy assignable");
	static_assert(std::is_same_v<decltype(std::declval<std::hash<Key>>()(std::declval<Key const &>())), size_t>, "std::hash<Key>() must return size_t");
}

// Base class for std::hash<lpg::optional<T>>:
// If std::hash<std::remove_const_t<T>> is enabled, it provides operator() to
// compute the hash; Otherwise, it is disabled.
template<typename T, typename = size_t>
struct optional_hash_base {
	optional_hash_base() = delete;
	optional_hash_base(const optional_hash_base &) = delete;
	optional_hash_base(optional_hash_base &&) = delete;
	optional_hash_base &operator=(const optional_hash_base &) = delete;
	optional_hash_base &operator=(optional_hash_base &&) = delete;
};

template<typename T>
struct optional_hash_base<T, decltype(std::hash<std::remove_const_t<T>>()(std::declval<std::remove_const_t<T>>()))> {
	using argument_type = lpg::optional<T>;
	using result_type = size_t;

	result_type operator()(const lpg::optional<T> &opt) const {
		lpg::internal::assert_hash_enabled<std::remove_const_t<T>>();
		return static_cast<bool>(opt) ? std::hash<std::remove_const_t<T>>()(*opt) : static_cast<result_type>(0);
	}
};
}// namespace internal
}// namespace lpg
#endif//OPTIONAL_INTERNAL_OPTIONAL_HASH_BASE_H_
