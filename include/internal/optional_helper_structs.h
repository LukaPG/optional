//
// Created by Luka Prebil Grintal on 21. 04. 20.
//

#ifndef OPTIONAL__OPTIONAL_HELPER_STRUCTS_H_
#define OPTIONAL__OPTIONAL_HELPER_STRUCTS_H_

namespace lpg {

/// A tag type to represent an empty optional
struct nullopt_t {
	class never_use {};
	constexpr explicit nullopt_t(never_use, never_use) noexcept {}
};
///  A tag type to tell optional to construct its value in-place
struct in_place_t {
	explicit in_place_t() = default;
};

/// A tag to tell optional to construct its value in-place
static constexpr in_place_t in_place{};
/// A tag to represent an empty optional
static constexpr nullopt_t nullopt{nullopt_t::never_use(), nullopt_t::never_use()};

} // end namespace lpg

#endif//OPTIONAL__OPTIONAL_HELPER_STRUCTS_H_
