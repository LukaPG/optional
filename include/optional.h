#pragma once
#include <compare>
#include <iostream>
#include <type_traits>
#include <utility>

#include "bad_optional_access.h"
#include "internal/optional_hash_base.h"
#include "internal/optional_helper_structs.h"

// TODO simplify SFINAE on everything but the assignment from optional operators

namespace lpg {
template<class T>
class optional {
   private:
	T value_;
	bool has_value_ = false;

	void clear() noexcept {
		if (this->has_value_) {
			this->value_.T::~T();
			this->has_value_ = false;
		}
	}

   public:
#pragma region Constructors
	constexpr optional() noexcept : value_(), has_value_(false){};
	constexpr optional(nullopt_t) noexcept : optional() {}
	constexpr optional(const optional &other) noexcept = default;// Trivial copy constructor
	constexpr optional(optional &&other) noexcept(std::is_nothrow_move_constructible_v<T>) = default;     // Trivial move constructor

	// Converting copy constructor
	// Explicit if there isn't a conversion from const U& to T
	template<
		class U,

		// sfinae definitions for when to compile
		class = typename std::enable_if<std::is_constructible_v<T, const U &>, T>::type,

		class = typename std::enable_if<!std::is_constructible_v<T, optional<U> &>>::type, class = typename std::enable_if<!std::is_constructible_v<T, const optional<U> &>>::type,
		class = typename std::enable_if<!std::is_constructible_v<T, optional<U> &&>>::type,
		class = typename std::enable_if<!std::is_constructible_v<T, const optional<U> &&>>::type,

		class = typename std::enable_if<!std::is_convertible_v<optional<U> &, T>>::type, class = typename std::enable_if<!std::is_convertible_v<const optional<U> &, T>>::type,
		class = typename std::enable_if<!std::is_convertible_v<optional<U> &&, T>>::type, class = typename std::enable_if<!std::is_convertible_v<const optional<U> &&, T>>::type>
	explicit(!std::is_convertible_v<const U &, T>) optional(const optional<U> &other) : optional() {
		if (other) {
			this->value_ = *other;
			this->has_value_ = true;
		}
	}

	// Converting move constructor
	// Explicit if there isn't a conversion from U&& to T
	template<
		class U,

		// sfinae definitions for when to compile
		class = typename std::enable_if<std::is_constructible_v<T, const U &>, T>::type,

		class = typename std::enable_if<!std::is_constructible_v<T, optional<U> &>>::type, class = typename std::enable_if<!std::is_constructible_v<T, const optional<U> &>>::type,
		class = typename std::enable_if<!std::is_constructible_v<T, optional<U> &&>>::type,
		class = typename std::enable_if<!std::is_constructible_v<T, const optional<U> &&>>::type,

		class = typename std::enable_if<!std::is_constructible_v<optional<U> &, T>>::type, class = typename std::enable_if<!std::is_constructible_v<const optional<U> &, T>>::type,
		class = typename std::enable_if<!std::is_constructible_v<optional<U> &&, T>>::type,
		class = typename std::enable_if<!std::is_constructible_v<const optional<U> &&, T>>::type>
	explicit(!std::is_convertible_v<U &&, T>) optional(optional<U> &&other) {
		if (other) {
			this->value_ = std::move(*other);
			this->has_value_ = true;
		}
	}

	// In place constructor
	template<class... Args,

			 // sfinae definitions for when to compile
			 class = typename std::enable_if<std::is_constructible_v<T, Args...>, T>::type>
	explicit constexpr optional(in_place_t, Args &&... args) : value_(std::forward<Args>(args)...), has_value_(true) {}

	// In place constructor from initializer list
	template<class U, class... Args,

			 // sfinae definitions for when to compile
			 class = typename std::enable_if<std::is_constructible_v<T, std::initializer_list<U>, Args...>, T>::type>
	explicit constexpr optional(in_place_t, std::initializer_list<U> ilist, Args &&... args) : value_(ilist, std::forward<Args>(args)...), has_value_(true) {}

	template<class U = T,

			 // sfinae definitions for when to compile
			 class = typename std::enable_if<std::is_constructible_v<T, U &&>, T>::type,
			 class = typename std::enable_if<!std::is_same<std::remove_cvref_t<U>, nullopt_t>::value>::type>
	explicit(!std::is_convertible_v<U &&, T>) constexpr optional(U &&value) : optional(in_place, std::forward<U &&>(value)) {}

	~optional() = default;
#pragma endregion
#pragma region operator=
	optional &operator=(nullopt_t) noexcept {
		this->clear();
		return *this;
	}

	constexpr optional &operator=(const optional &other) = default;
	constexpr optional &operator=(optional &&other) noexcept(std::is_nothrow_move_assignable_v<T> && std::is_nothrow_move_constructible_v<T>) = default;

	template<
		class U = T,

		// SFINAE
		std::enable_if_t<
			!std::is_same_v<
				lpg::optional<T>,
				std::decay_t<U>> && !std::conjunction_v<std::is_scalar_v<T>, std::is_same_v<T, std::decay_t<U>>> && std::is_constructible_v<T, U> && std::is_assignable_v<T &, U>>>
	optional &operator=(U &&rhs) {
		if (has_value_) {
			this->value_ = std::forward<U>(rhs);
		} else {
			new (std::addressof(this->value_)) T(std::forward<U>(rhs));
			has_value_ = true;
		}
		return *this;
	}

	template<class U,

			 // SFINAE
			 std::enable_if_t<std::is_constructible<T, const U &>::value && std::is_assignable<T &, const U &>::value &&

							  !std::is_constructible<T, lpg::optional<U> &>::value && !std::is_constructible<T, lpg::optional<U> &&>::value
							  && !std::is_constructible<T, const lpg::optional<U> &>::value && !std::is_constructible<T, const lpg::optional<U> &&>::value
							  && !std::is_convertible<lpg::optional<U> &, T>::value && !std::is_convertible<lpg::optional<U> &&, T>::value
							  && !std::is_convertible<const lpg::optional<U> &, T>::value && !std::is_convertible<const lpg::optional<U> &&, T>::value
							  && !std::is_assignable<T &, lpg::optional<U> &>::value && !std::is_assignable<T &, lpg::optional<U> &&>::value
							  && !std::is_assignable<T &, const lpg::optional<U> &>::value && !std::is_assignable<T &, const lpg::optional<U> &&>::value>>
	optional &operator=(const optional<U> &rhs) {
		if (!rhs) {
			this->clear();
			return *this;
		} else {
			this->value_ = *rhs;
			this->has_value_ = true;
			return *this;
		}
	}

	template<class U,

			 // SFINAE
			 std::enable_if_t<std::is_constructible<T, U>::value && std::is_assignable<T &, U>::value &&

							  !std::is_constructible<T, lpg::optional<U> &>::value && !std::is_constructible<T, lpg::optional<U> &&>::value
							  && !std::is_constructible<T, const lpg::optional<U> &>::value && !std::is_constructible<T, const lpg::optional<U> &&>::value
							  && !std::is_convertible<lpg::optional<U> &, T>::value && !std::is_convertible<lpg::optional<U> &&, T>::value
							  && !std::is_convertible<const lpg::optional<U> &, T>::value && !std::is_convertible<const lpg::optional<U> &&, T>::value
							  && !std::is_assignable<T &, lpg::optional<U> &>::value && !std::is_assignable<T &, lpg::optional<U> &&>::value
							  && !std::is_assignable<T &, const lpg::optional<U> &>::value && !std::is_assignable<T &, const lpg::optional<U> &&>::value>>
	optional &operator=(optional<U> &&rhs) {
		if (!rhs) {
			this->clear();
			return *this;
		} else {
			this->value_ = std::move(*rhs);
			this->has_value_ = true;
			return *this;
		}
	}
#pragma endregion
#pragma region operator* / operator->
	// Exhaustive operator */-> overload set
	constexpr const T &&operator*() const && { return std::move(this->value_); }
	constexpr T &&operator*() && { return std::move(this->value_); }
	constexpr const T &operator*() const & { return this->value_; }
	constexpr T &operator*() & { return this->value_; }

	constexpr const T *operator->() const { return std::addressof(this->value_); }
	constexpr T *operator->() { return std::addressof(this->value_); }
#pragma endregion
#pragma region this->value[_or]()
	// Value access with check
	constexpr T &value() & {
		if (this->has_value_) return value_;
		throw lpg::bad_optional_access();
	}
	constexpr const T &value() const & {
		if (this->has_value_) return value_;
		throw lpg::bad_optional_access();
	}
	constexpr T &&value() && {
		if (this->has_value_) return std::move(value_);
		throw lpg::bad_optional_access();
	}
	constexpr const T &&value() const && {
		if (this->has_value_) return std::move(value_);
		throw lpg::bad_optional_access();
	}

	template<class U>
	constexpr T value_or(U &&default_value) const & {
		if (this->has_value_) return value_;
		return static_cast<T>(std::forward<U>(default_value));
	}

	template<class U>
	constexpr T value_or(U &&default_value) && {
		if (this->has_value_) return std::move(value_);
		return static_cast<T>(std::forward<U>(default_value));
	}
#pragma endregion

	constexpr explicit operator bool() const noexcept { return this->has_value_; }
	[[nodiscard]] constexpr bool has_value() const noexcept { return this->has_value_; }

	void swap(lpg::optional<T> &rhs) noexcept(std::is_nothrow_move_constructible<T>::value &&std::is_nothrow_swappable<T>::value) {
		if (!this->has_value_ && !rhs) { return; }
		if (this->has_value_) {
			if (rhs) {
				std::swap(**this, *rhs);
			} else {
				new (std::addressof(rhs.value_)) T(std::move(this->value_));
				rhs.has_value_ = true;
				this->clear();
			}
		} else if (rhs) {
			new (std::addressof(this->value_)) T(std::move(rhs.value_));
			this->has_value_ = true;
			rhs.clear();
		}
	}

	void reset() noexcept { this->clear(); }

	template<typename... Args>
	T &emplace(Args &&... args) {
		static_assert(std::is_constructible<T, Args &&...>::value, "T must be constructable from Args");

		this->clear();

		new (std::addressof(this->value_)) T(std::forward<Args &&>(args)...);
		this->has_value_ = true;

		return this->value();
	}
};

#pragma region operator==, operator!=, operator<, operator<=, operator>, operator>=, operator<=>

template<class T, class U>
constexpr bool operator==(const optional<T> &lhs, const optional<U> &rhs) {
	if (lhs.has_value() != rhs.has_value()) return false;
	else if (!lhs.has_value())
		return true;
	else
		return *lhs == *rhs;
}

template<class T, class U>
constexpr bool operator!=(const optional<T> &lhs, const optional<U> &rhs) {
	if (lhs.has_value() != rhs.has_value()) return true;
	else if (!lhs.has_value())
		return false;
	else
		return *lhs != *rhs;
}

template<class T, class U>
constexpr bool operator<(const optional<T> &lhs, const optional<U> &rhs) {
	if (!rhs) return true;
	if (!lhs) return false;
	return *lhs < *rhs;
}

template<class T, class U>
constexpr bool operator<=(const optional<T> &lhs, const optional<U> &rhs) {
	if (!lhs) return false;
	if (!rhs) return true;
	return *lhs <= *rhs;
}

template<class T, class U>
constexpr bool operator>(const optional<T> &lhs, const optional<U> &rhs) {
	if (!rhs) return false;
	if (!lhs) return true;
	return *lhs > *rhs;
}

template<class T, class U>
constexpr bool operator>=(const optional<T> &lhs, const optional<U> &rhs) {
	if (!lhs) return true;
	if (!rhs) return false;
	return *lhs >= *rhs;
}

#if 0 // TODO implement comparison through <=> once there is full support for concepts
template<class T>
constexpr std::strong_ordering operator<=>(const optional<T> &opt, const lpg::nullopt_t) noexcept {
	return static_cast<bool>(opt) <=> false;
}

template<class T>
constexpr std::strong_ordering operator<=>(const lpg::nullopt_t, const optional<T> &opt) noexcept {
	return false <=> static_cast<bool>(opt);
}
#else
template<typename T>
constexpr bool operator==(const lpg::optional<T> &opt, const lpg::nullopt_t) noexcept {
	return !opt;
}

template<typename T>
constexpr bool operator==(const lpg::nullopt_t, const lpg::optional<T> &opt) noexcept {
	return !opt;
}

template<typename T>
constexpr bool operator!=(const lpg::optional<T> &opt, const lpg::nullopt_t) noexcept {
	return static_cast<bool>(opt);
}

template<typename T>
constexpr bool operator!=(const lpg::nullopt_t, const lpg::optional<T> &opt) noexcept {
	return static_cast<bool>(opt);
}

template<typename T>
constexpr bool operator<(const lpg::optional<T> &opt, const lpg::nullopt_t) noexcept {
	return false;
}

template<typename T>
constexpr bool operator<(const lpg::nullopt_t, const lpg::optional<T> &opt) noexcept {
	return static_cast<bool>(opt);
}

template<typename T>
constexpr bool operator<=(const lpg::optional<T> &opt, const lpg::nullopt_t) noexcept {
	return !opt;
}

template<typename T>
constexpr bool operator<=(const lpg::nullopt_t, const lpg::optional<T> &opt) noexcept {
	return true;
}

template<typename T>
constexpr bool operator>(const lpg::optional<T> &opt, const lpg::nullopt_t) noexcept {
	return static_cast<bool>(opt);
}

template<typename T>
constexpr bool operator>(const lpg::nullopt_t, const lpg::optional<T> &opt) noexcept {
	return false;
}

template<typename T>
constexpr bool operator>=(const lpg::optional<T> &opt, const lpg::nullopt_t) noexcept {
	return true;
}

template<typename T>
constexpr bool operator>=(const lpg::nullopt_t, const lpg::optional<T> &opt) noexcept {
	return !opt;
}
#endif

template<class T, class U>
constexpr bool operator==(const optional<T> &opt, const U &value) {
	return static_cast<bool>(opt) ? *opt == value : false;
}

template<class T, class U>
constexpr bool operator==(const T &value, const optional<U> &opt) {
	return static_cast<bool>(opt) ? value == *opt : false;
}

template<class T, class U>
constexpr bool operator!=(const optional<T> &opt, const U &value) {
	return static_cast<bool>(opt) ? *opt != value : true;
}

template<class T, class U>
constexpr bool operator!=(const T &value, const optional<U> &opt) {
	return static_cast<bool>(opt) ? value != *opt : true;
}

template<class T, class U>
constexpr bool operator<(const optional<T> &opt, const U &value) {
	return static_cast<bool>(opt) ? *opt < value : true;
}

template<class T, class U>
constexpr bool operator<(const T &value, const optional<U> &opt) {
	return static_cast<bool>(opt) ? value < *opt : false;
}

template<class T, class U>
constexpr bool operator<=(const optional<T> &opt, const U &value) {
	return static_cast<bool>(opt) ? *opt <= value : true;
}

template<class T, class U>
constexpr bool operator<=(const T &value, const optional<U> &opt) {
	return static_cast<bool>(opt) ? value <= *opt : false;
}

template<class T, class U>
constexpr bool operator>(const optional<T> &opt, const U &value) {
	return static_cast<bool>(opt) ? *opt > value : false;
}

template<class T, class U>
constexpr bool operator>(const T &value, const optional<U> &opt) {
	return static_cast<bool>(opt) ? value > *opt : true;
}

template<class T, class U>
constexpr bool operator>=(const optional<T> &opt, const U &value) {
	return static_cast<bool>(opt) ? *opt >= value : false;
}

template<class T, class U>
constexpr bool operator>=(const T &value, const optional<U> &opt) {
	return static_cast<bool>(opt) ? value >= *opt : true;
}

#pragma endregion
#pragma region make_optional
/**
 * Creates an instance of lpg::optional from value
 * @tparam T - type of optional value
 * @param value - value to construct the optional of
 * @return lpg::optional<T> containing value
 */
template<typename T>
[[maybe_unused]] constexpr lpg::optional<std::decay_t<T>> make_optional(T &&value) {
	return lpg::optional<std::decay_t<T>>(std::forward<T>(value));
}

/**
 * Creates lpg::optional with its value constructed in place, from provided parameters.
 * @tparam T - type of optional value
 * @tparam Args - parameter pack of arguments for the constructor of T
 * @param args - arguments for the constructor of T
 * @return - lpg::optional<T> with its value constructed in place
 */
template<typename T, typename... Args>
[[maybe_unused]] constexpr lpg::optional<T> make_optional(Args &&... args) {
	return lpg::optional<T>(lpg::in_place, std::forward<Args>(args)...);
}

/**
 * Creates lpg::optional, with its value constructed in place,
 * from arguments provided via std::initializer_list
 * @tparam T - type of optional value
 * @tparam U - type of initializer list
 * @tparam Args - parameter pack of arguments for the constructor of T
 * @param il - initializer list for T
 * @param args - arguments for the constructor of T
 * @return lpg::optional<T> with its value constructed in place
 */
template<typename T, typename U, typename... Args>
[[maybe_unused]] constexpr lpg::optional<T> make_optional(std::initializer_list<U> il, Args &&... args) {
	return lpg::optional<T>(lpg::in_place, il, std::forward<Args>(args)...);
}

#pragma endregion
#pragma region swap

/**
 * Swaps values of two optional objects using standard semantics
 * @tparam T - type of optional objects
 * @param lhs - first optional<T>
 * @param rhs - second optional<T>
 */
template<typename T, std::enable_if_t<std::is_move_constructible_v<T> && std::is_swappable_v<T>>>
void swap(lpg::optional<T> &lhs, lpg::optional<T> &rhs) noexcept(noexcept(lhs.swap(rhs))) {
	lhs.swap(rhs);
}
#pragma endregion

}// namespace lpg

#pragma region std::hash<lpg::optional<T>>
namespace std {

template<typename T>
struct hash<lpg::optional<T>> : public lpg::internal::optional_hash_base<T> {};

}// namespace std
#pragma endregion
