# `lpg::optional`
Standard compliant (C++20), header only, `optional` implementation with [P0798](https://wg21.tartanllama.xyz/monadic-optional) described monadic operations.

Testing suite provided by Sy Brand's [`tl::optional`](https://tl.tartanllama.xyz/en/latest/api/optional.html) project, with added tests for `std::hash<lpg::optional>` and removed tests for `optional<T&>`
## Dependencies
* No runtime dependencies
* `Catch2` for testing, installed via vcpkg and supplying `DCMAKE_TOOLCHAIN_FILE`

### DISCLAIMER
* Depends on C++20 functionality that may not be
implemented on all compilers yet. Tested with Clang-11 and libc++-11
* Monadic operations not yet implemented
