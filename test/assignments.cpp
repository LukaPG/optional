//
// Created by luka on 26/04/2020.
//
#include "catch.hpp"
#include "../include/optional.h"

TEST_CASE("Value assignment", "[assign.value]"){
	lpg::optional<int> o1 = 10;
	lpg::optional<int> o2 = 15;
	lpg::optional<int> o3;

	REQUIRE(*o1 == 10);

	o1 = o1;
	REQUIRE(*o1 == 10);

	o1 = o2;
	REQUIRE(*o1 == 15);

	o1 = o3;
	REQUIRE(!o1);

	o1 = 100;
	REQUIRE(*o1 == 100);

	o1 = lpg::nullopt;
	REQUIRE(!o1);

	o1 = std::move(o2);
	REQUIRE(*o1 == 15);

	lpg::optional<long> o4 = 500l;
	o1 = o4;
	REQUIRE(*o1 == 500);

	o1 = lpg::nullopt;
	o1 = std::move(o4);
	REQUIRE(*o1 == 500);
}