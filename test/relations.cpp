//
// Created by luka on 26/04/2020.
//
#include "catch.hpp"
#include "../include/optional.h"
#include <string>

TEST_CASE("Relational ops", "[relops]") {
	lpg::optional<int> o1{4};
	lpg::optional<int> o2{42};
	lpg::optional<int> o3{};

	SECTION("self simple") {
		REQUIRE(!(o1 == o2));
		REQUIRE(o1 == o1);
		REQUIRE(o1 != o2);
		REQUIRE(!(o1 != o1));
		REQUIRE(o1 < o2);
		REQUIRE(!(o1 < o1));
		REQUIRE(!(o1 > o2));
		REQUIRE(!(o1 > o1));
		REQUIRE(o1 <= o2);
		REQUIRE(o1 <= o1);
		REQUIRE(!(o1 >= o2));
		REQUIRE(o1 >= o1);
	}

	SECTION("nullopt simple") {
		REQUIRE(!(o1 == lpg::nullopt));
		REQUIRE(!(lpg::nullopt == o1));
		REQUIRE(o1 != lpg::nullopt);
		REQUIRE(lpg::nullopt != o1);
		REQUIRE(!(o1 < lpg::nullopt));
		REQUIRE(lpg::nullopt < o1);
		REQUIRE(o1 > lpg::nullopt);
		REQUIRE(!(lpg::nullopt > o1));
		REQUIRE(!(o1 <= lpg::nullopt));
		REQUIRE(lpg::nullopt <= o1);
		REQUIRE(o1 >= lpg::nullopt);
		REQUIRE(!(lpg::nullopt >= o1));

		REQUIRE(o3 == lpg::nullopt);
		REQUIRE(lpg::nullopt == o3);
		REQUIRE(!(o3 != lpg::nullopt));
		REQUIRE(!(lpg::nullopt != o3));
		REQUIRE(!(o3 < lpg::nullopt));
		REQUIRE(!(lpg::nullopt < o3));
		REQUIRE(!(o3 > lpg::nullopt));
		REQUIRE(!(lpg::nullopt > o3));
		REQUIRE(o3 <= lpg::nullopt);
		REQUIRE(lpg::nullopt <= o3);
		REQUIRE(o3 >= lpg::nullopt);
		REQUIRE(lpg::nullopt >= o3);
	}

	SECTION("with T simple") {
		REQUIRE(!(o1 == 1));
		REQUIRE(!(1 == o1));
		REQUIRE(o1 != 1);
		REQUIRE(1 != o1);
		REQUIRE(!(o1 < 1));
		REQUIRE(1 < o1);
		REQUIRE(o1 > 1);
		REQUIRE(!(1 > o1));
		REQUIRE(!(o1 <= 1));
		REQUIRE(1 <= o1);
		REQUIRE(o1 >= 1);
		REQUIRE(!(1 >= o1));

		REQUIRE(o1 == 4);
		REQUIRE(4 == o1);
		REQUIRE(!(o1 != 4));
		REQUIRE(!(4 != o1));
		REQUIRE(!(o1 < 4));
		REQUIRE(!(4 < o1));
		REQUIRE(!(o1 > 4));
		REQUIRE(!(4 > o1));
		REQUIRE(o1 <= 4);
		REQUIRE(4 <= o1);
		REQUIRE(o1 >= 4);
		REQUIRE(4 >= o1);
	}

	lpg::optional<std::string> o4{"hello"};
	lpg::optional<std::string> o5{"xyz"};

	SECTION("self complex") {
		REQUIRE(!(o4 == o5));
		REQUIRE(o4 == o4);
		REQUIRE(o4 != o5);
		REQUIRE(!(o4 != o4));
		REQUIRE(o4 < o5);
		REQUIRE(!(o4 < o4));
		REQUIRE(!(o4 > o5));
		REQUIRE(!(o4 > o4));
		REQUIRE(o4 <= o5);
		REQUIRE(o4 <= o4);
		REQUIRE(!(o4 >= o5));
		REQUIRE(o4 >= o4);
	}

	SECTION("nullopt complex") {
		REQUIRE(!(o4 == lpg::nullopt));
		REQUIRE(!(lpg::nullopt == o4));
		REQUIRE(o4 != lpg::nullopt);
		REQUIRE(lpg::nullopt != o4);
		REQUIRE(!(o4 < lpg::nullopt));
		REQUIRE(lpg::nullopt < o4);
		REQUIRE(o4 > lpg::nullopt);
		REQUIRE(!(lpg::nullopt > o4));
		REQUIRE(!(o4 <= lpg::nullopt));
		REQUIRE(lpg::nullopt <= o4);
		REQUIRE(o4 >= lpg::nullopt);
		REQUIRE(!(lpg::nullopt >= o4));

		REQUIRE(o3 == lpg::nullopt);
		REQUIRE(lpg::nullopt == o3);
		REQUIRE(!(o3 != lpg::nullopt));
		REQUIRE(!(lpg::nullopt != o3));
		REQUIRE(!(o3 < lpg::nullopt));
		REQUIRE(!(lpg::nullopt < o3));
		REQUIRE(!(o3 > lpg::nullopt));
		REQUIRE(!(lpg::nullopt > o3));
		REQUIRE(o3 <= lpg::nullopt);
		REQUIRE(lpg::nullopt <= o3);
		REQUIRE(o3 >= lpg::nullopt);
		REQUIRE(lpg::nullopt >= o3);
	}

	SECTION("with T complex") {
		REQUIRE(!(o4 == "a"));
		REQUIRE(!("a" == o4));
		REQUIRE(o4 != "a");
		REQUIRE("a" != o4);
		REQUIRE(!(o4 < "a"));
		REQUIRE("a" < o4);
		REQUIRE(o4 > "a");
		REQUIRE(!("a" > o4));
		REQUIRE(!(o4 <= "a"));
		REQUIRE("a" <= o4);
		REQUIRE(o4 >= "a");
		REQUIRE(!("a" >= o4));

		REQUIRE(o4 == "hello");
		REQUIRE("hello" == o4);
		REQUIRE(!(o4 != "hello"));
		REQUIRE(!("hello" != o4));
		REQUIRE(!(o4 < "hello"));
		REQUIRE(!("hello" < o4));
		REQUIRE(!(o4 > "hello"));
		REQUIRE(!("hello" > o4));
		REQUIRE(o4 <= "hello");
		REQUIRE("hello" <= o4);
		REQUIRE(o4 >= "hello");
		REQUIRE("hello" >= o4);
	}
}