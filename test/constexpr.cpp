//
// Created by luka on 26/04/2020.
//

#include "catch.hpp"
#include "../include/optional.h"

TEST_CASE("Constexpr", "[constexpr]") {
	SECTION("empty construct") {
    constexpr lpg::optional<int> o2{};
    constexpr lpg::optional<int> o3 = {};
    constexpr lpg::optional<int> o4 = lpg::nullopt;
    constexpr lpg::optional<int> o5 = {lpg::nullopt};
    constexpr lpg::optional<int> o6(lpg::nullopt);

    STATIC_REQUIRE(!o2);
    STATIC_REQUIRE(!o3);
    STATIC_REQUIRE(!o4);
    STATIC_REQUIRE(!o5);
    STATIC_REQUIRE(!o6);
  }

  SECTION("value construct") {
    constexpr lpg::optional<int> o1 = 42;
    constexpr lpg::optional<int> o2{42};
    constexpr lpg::optional<int> o3(42);
    constexpr lpg::optional<int> o4 = {42};
    constexpr static int i = 42;
    constexpr lpg::optional<int> o5 = std::move(i);
    constexpr lpg::optional<int> o6{std::move(i)};
    constexpr lpg::optional<int> o7(std::move(i));
    constexpr lpg::optional<int> o8 = {std::move(i)};

    STATIC_REQUIRE(*o1 == 42);
    STATIC_REQUIRE(*o2 == 42);
    STATIC_REQUIRE(*o3 == 42);
    STATIC_REQUIRE(*o4 == 42);
    STATIC_REQUIRE(*o5 == 42);
    STATIC_REQUIRE(*o6 == 42);
    STATIC_REQUIRE(*o7 == 42);
    STATIC_REQUIRE(*o8 == 42);
  }
}
