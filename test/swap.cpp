//
// Created by luka on 27/04/2020.
//
#include "catch.hpp"
#include "../include/optional.h"

TEST_CASE("Swap value", "[swap.value]") {
	lpg::optional<int> o1 = 42;
	lpg::optional<int> o2 = 12;
	o1.swap(o2);
	CHECK(o1.value() == 12);
	CHECK(o2.value() == 42);
}

TEST_CASE("Swap value with null initialized", "[swap.value_nullopt]") {
	lpg::optional<int> o1 = 42;
	lpg::optional<int> o2 = lpg::nullopt;
	o1.swap(o2);
	CHECK(!o1.has_value());
	CHECK(o2.value() == 42);
}

TEST_CASE("Swap null initialized with value", "[swap.nullopt_value]") {
	lpg::optional<int> o1 = lpg::nullopt;
	lpg::optional<int> o2 = 42;
	o1.swap(o2);
	CHECK(o1.value() == 42);
	CHECK(!o2.has_value());
}