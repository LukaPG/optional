//
// Created by luka on 26/04/2020.
//
#include <vector>
#include "catch.hpp"
#include "../include/optional.h"

struct test_helper {
	test_helper() = default;
	test_helper(test_helper &) = delete;
	test_helper(test_helper &&) noexcept {};
};

TEST_CASE("Constructors", "[constructors]") {
	lpg::optional<int> o1;
	REQUIRE(!o1);

	lpg::optional<int> o2 = lpg::nullopt;
	REQUIRE(!o2);

	lpg::optional<int> o3 = 42;
	REQUIRE(*o3 == 42);

	lpg::optional<int> o4 = o3;
	REQUIRE(*o4 == 42);

	lpg::optional<int> o5 = o1;
	REQUIRE(!o5);

	lpg::optional<int> o6 = std::move(o3);
	REQUIRE(*o6 == 42);

	lpg::optional<short> o7 = 42;
	REQUIRE(*o7 == 42);

	lpg::optional<int> o8 = o7;
	REQUIRE(*o8 == 42);

	lpg::optional<int> o9 = std::move(o7);
	REQUIRE(*o9 == 42);

	std::vector<test_helper> v;
	v.emplace_back();
	lpg::optional<std::vector<test_helper>> ov = std::move(v);
	REQUIRE(!ov->empty());
}