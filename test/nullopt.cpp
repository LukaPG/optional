//
// Created by luka on 26/04/2020.
//
#include "catch.hpp"
#include "../include/optional.h"

TEST_CASE("Nullopt", "[nullopt]") {
	lpg::optional<int> o1 = lpg::nullopt;
	lpg::optional<int> o2{lpg::nullopt};
	lpg::optional<int> o3(lpg::nullopt);
	lpg::optional<int> o4 = {lpg::nullopt};

	REQUIRE(!o1);
	REQUIRE(!o2);
	REQUIRE(!o3);
	REQUIRE(!o4);

	REQUIRE_FALSE(std::is_default_constructible_v<lpg::nullopt_t>);
}