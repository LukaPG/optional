//
// Created by luka on 26/04/2020.
//
#include "catch.hpp"
#include "../include/optional.h"

TEST_CASE("Triviality", "[bases.triviality]") {
REQUIRE(std::is_trivially_copy_constructible_v<lpg::optional<int>>);
REQUIRE(std::is_trivially_copy_assignable_v<lpg::optional<int>>);
REQUIRE(std::is_trivially_move_constructible_v<lpg::optional<int>>);
REQUIRE(std::is_trivially_move_assignable_v<lpg::optional<int>>);
REQUIRE(std::is_trivially_destructible_v<lpg::optional<int>>);

{
	struct T {
		T(const T&) = default;
		T(T&&) = default;
		T& operator=(const T&) = default;
		T& operator=(T&&) = default;
		~T() = default;
	};
	REQUIRE(std::is_trivially_copy_constructible_v<lpg::optional<T>>);
	REQUIRE(std::is_trivially_copy_assignable_v<lpg::optional<T>>);
	REQUIRE(std::is_trivially_move_constructible_v<lpg::optional<T>>);
	REQUIRE(std::is_trivially_move_assignable_v<lpg::optional<T>>);
	REQUIRE(std::is_trivially_destructible_v<lpg::optional<T>>);
}

{
	struct T {
		T(const T&){}
		T(T&&) {};
		T& operator=(const T&) { return *this; }
		T& operator=(T&&) { return *this; };
		~T(){}
	};
	REQUIRE(!std::is_trivially_copy_constructible_v<lpg::optional<T>>);
	REQUIRE(!std::is_trivially_copy_assignable_v<lpg::optional<T>>);
	REQUIRE(!std::is_trivially_move_constructible_v<lpg::optional<T>>);
	REQUIRE(!std::is_trivially_move_assignable_v<lpg::optional<T>>);
	REQUIRE(!std::is_trivially_destructible_v<lpg::optional<T>>);
}

}

TEST_CASE("Deletion", "[bases.deletion]") {
	REQUIRE(std::is_copy_constructible_v<lpg::optional<int>>);
	REQUIRE(std::is_copy_assignable_v<lpg::optional<int>>);
	REQUIRE(std::is_move_constructible_v<lpg::optional<int>>);
	REQUIRE(std::is_move_assignable_v<lpg::optional<int>>);
	REQUIRE(std::is_destructible_v<lpg::optional<int>>);

	{
		struct T {
			T(const T&) = default;
			T(T&&) = default;
			T& operator=(const T&) = default;
			T& operator=(T&&) = default;
			~T() = default;
		};
		REQUIRE(std::is_copy_constructible_v<lpg::optional<T>>);
		REQUIRE(std::is_copy_assignable_v<lpg::optional<T>>);
		REQUIRE(std::is_move_constructible_v<lpg::optional<T>>);
		REQUIRE(std::is_move_assignable_v<lpg::optional<T>>);
		REQUIRE(std::is_destructible_v<lpg::optional<T>>);
	}

	{
		struct T {
			T(const T&)=delete;
			T(T&&)=delete;
			T& operator=(const T&)=delete;
			T& operator=(T&&)=delete;
		};
		REQUIRE(!std::is_copy_constructible_v<lpg::optional<T>>);
		REQUIRE(!std::is_copy_assignable_v<lpg::optional<T>>);
		REQUIRE(!std::is_move_constructible_v<lpg::optional<T>>);
		REQUIRE(!std::is_move_assignable_v<lpg::optional<T>>);
	}

	{
		struct T {
			T(const T&)=delete;
			T(T&&)=default;
			T& operator=(const T&)=delete;
			T& operator=(T&&)=default;
		};
		REQUIRE(!std::is_copy_constructible_v<lpg::optional<T>>);
		REQUIRE(!std::is_copy_assignable_v<lpg::optional<T>>);
		REQUIRE(std::is_move_constructible_v<lpg::optional<T>>);
		REQUIRE(std::is_move_assignable_v<lpg::optional<T>>);
	}

	{
		struct T {
			T(const T&)=default;
			T(T&&)=delete;
			T& operator=(const T&)=default;
			T& operator=(T&&)=delete;
		};
		REQUIRE(std::is_copy_constructible_v<lpg::optional<T>>);
		REQUIRE(std::is_copy_assignable_v<lpg::optional<T>>);
	}
}