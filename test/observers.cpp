//
// Created by luka on 26/04/2020.
//

#include "catch.hpp"
#include "../include/optional.h"

struct move_detector {
	move_detector() = default;
	move_detector(move_detector &&rhs)  noexcept { rhs.been_moved = true; }
	bool been_moved = false;
};

TEST_CASE("Observers", "[observers]") {
	lpg::optional<int> o1 = 42;
	lpg::optional<int> o2;
	const lpg::optional<int> o3 = 42;

	REQUIRE(*o1 == 42);
	REQUIRE(*o1 == o1.value());
	REQUIRE(o2.value_or(42) == 42);
	REQUIRE(o3.value() == 42);
	REQUIRE(std::is_same_v<decltype(o1.value()), int &>);
	REQUIRE(std::is_same_v<decltype(o3.value()), const int &>);
	REQUIRE(std::is_same_v<decltype(std::move(o1).value()), int &&>);
	REQUIRE(std::is_same_v<decltype(std::move(o3).value()), const int &&>);

	lpg::optional<move_detector> o4{lpg::in_place};
	move_detector o5 = std::move(o4).value();
	REQUIRE(o4->been_moved);
	REQUIRE(!o5.been_moved);
}