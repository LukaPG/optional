//
// Created by luka on 26/04/2020.
//
#include "catch.hpp"
#include "../include/optional.h"

TEST_CASE("Noexcept", "[noexcept]") {
	lpg::optional<int> o1{4};
	lpg::optional<int> o2{42};

	SECTION("comparison with nullopt") {
		REQUIRE(noexcept(o1 == lpg::nullopt));
		REQUIRE(noexcept(lpg::nullopt == o1));
		REQUIRE(noexcept(o1 != lpg::nullopt));
		REQUIRE(noexcept(lpg::nullopt != o1));
		REQUIRE(noexcept(o1 < lpg::nullopt));
		REQUIRE(noexcept(lpg::nullopt < o1));
		REQUIRE(noexcept(o1 <= lpg::nullopt));
		REQUIRE(noexcept(lpg::nullopt <= o1));
		REQUIRE(noexcept(o1 > lpg::nullopt));
		REQUIRE(noexcept(lpg::nullopt > o1));
		REQUIRE(noexcept(o1 >= lpg::nullopt));
		REQUIRE(noexcept(lpg::nullopt >= o1));
	}


	SECTION("swap") {
		REQUIRE(noexcept(std::swap(o1, o2)) == noexcept(o1.swap(o2)));

		struct nothrow_swappable {
			nothrow_swappable &swap(const nothrow_swappable &) noexcept {
				return *this;
			}
		};

		struct throw_swappable {
			throw_swappable() = default;
			throw_swappable(const throw_swappable &) noexcept(false) {}
			throw_swappable(throw_swappable &&) noexcept(false) {}
			throw_swappable &swap(const throw_swappable &) noexcept(false) { return *this; }
		};

		lpg::optional<nothrow_swappable> ont;
		lpg::optional<throw_swappable> ot;

		REQUIRE(noexcept(ont.swap(ont)));
		REQUIRE(!noexcept(ot.swap(ot)));
	}

	SECTION("constructors") {
		REQUIRE(noexcept(lpg::optional<int>{}));
		REQUIRE(noexcept(lpg::optional<int>{lpg::nullopt}));

		struct nothrow_move {
			nothrow_move(nothrow_move &&) noexcept = default;
		};

		struct throw_move {
			throw_move(throw_move &&) noexcept(false) {};
		};

		using nothrow_opt = lpg::optional<nothrow_move>;
		using throw_opt = lpg::optional<throw_move>;

		REQUIRE(std::is_nothrow_move_constructible_v<nothrow_opt>);
		REQUIRE(!std::is_nothrow_move_constructible_v<throw_opt>);
	}

	SECTION("Assignment") {
		REQUIRE(noexcept(o1 = lpg::nullopt));

		struct nothrow_move_assign {
			nothrow_move_assign() = default;
			nothrow_move_assign(nothrow_move_assign &&) noexcept = default;
			nothrow_move_assign &operator=(const nothrow_move_assign &) = default;
		};

		struct throw_move_assign {
			throw_move_assign() = default;
			throw_move_assign(throw_move_assign &&) noexcept(false) {};
			throw_move_assign &operator=(const throw_move_assign &) noexcept(false) { return *this; }
		};

		using nothrow_opt = lpg::optional<nothrow_move_assign>;
		using throw_opt = lpg::optional<throw_move_assign>;

		REQUIRE(noexcept(std::declval<nothrow_opt>() = std::declval<nothrow_opt>()));
		REQUIRE(!noexcept(std::declval<throw_opt>() = std::declval<throw_opt>()));
	}

	SECTION("Observers") {
		REQUIRE(noexcept(static_cast<bool>(o1)));
		REQUIRE(noexcept(o1.has_value()));
	}

	SECTION("Modifiers") {
		REQUIRE(noexcept(o1.reset()));
	}
}