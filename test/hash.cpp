//
// Created by luka on 26/04/2020.
//
#include "catch.hpp"
#include "../include/optional.h"
#include <set>
#include <string>

TEST_CASE("Hashing", "[hash]"){
	std::set<lpg::optional<std::string>> s;
	s.insert({"100"});
	s.insert(std::string("words in memory"));
	s.insert("something");
	s.insert(lpg::optional<std::string>("idk"));
	// TODO figure out why this doesn't work
	s.insert(lpg::optional<std::string>{});

	REQUIRE(s.size() == 5);
	REQUIRE(s.contains(lpg::optional<std::string>("100")));
	REQUIRE(s.contains("something"));
	REQUIRE(s.contains({"idk"}));
	// TODO figure out why this doesn't work
	REQUIRE_FALSE(s.contains(lpg::optional<std::string>{}));
}